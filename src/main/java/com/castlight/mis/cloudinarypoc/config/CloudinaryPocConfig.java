package com.castlight.mis.cloudinarypoc.config;

import com.cloudinary.Cloudinary;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.yml")
public class CloudinaryPocConfig {

  @Bean
  public Cloudinary buildCloudinary(
      @Value("${cloudinary.api_key}") String apiKey,
      @Value("${cloudinary.api_secret}") String apiSecret,
      @Value("${cloudinary.cloud_name}") String cloudName,
      @Value("${cloudinary.secure}") String secure) {
    Map<String, String> cloudinarySettings = new HashMap<>();
    cloudinarySettings.put("api_key", apiKey);
    cloudinarySettings.put("api_secret", apiSecret);
    cloudinarySettings.put("cloud_name", cloudName);
    cloudinarySettings.put("secure", secure);

//    return new Cloudinary("cloudinary://"+apiKey+":"+apiSecret+"@"+cloudName);

    return new Cloudinary(cloudinarySettings);
  }
}
