package com.castlight.mis.cloudinarypoc.controller;

import com.castlight.mis.cloudinarypoc.service.CloudinaryService;
import com.cloudinary.Cloudinary;
import com.cloudinary.api.ApiResponse;
import com.cloudinary.utils.ObjectUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.websocket.server.PathParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.cloudinary.json.JSONArray;
import org.cloudinary.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpServerErrorException.InternalServerError;
import org.springframework.web.multipart.MultipartFile;

@RestController
@Slf4j
@RequestMapping("images")
public class ImageController {

  @Autowired
  private CloudinaryService cloudinaryService;

  /**
   * GET image details for all the images stored in the Cloudinary
   */
  @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ApiResponse> getImageUrls() {
    Map response = cloudinaryService.getImages();
    return response != null ? new ResponseEntity(response, HttpStatus.OK)
        : new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * Get URL for the specific image by publidId
   */
  @GetMapping(value = "/{publicId}")
  public Map getImageUrls(@PathVariable("publicId") String publicId) {
    return cloudinaryService.getImage(publicId);
  }


  /**
   * Delete the image from Cloudinary
   */
  @DeleteMapping(value = "/{publicId}")
  public ResponseEntity deleteImage(@PathVariable("publicId") String publicId) {

    boolean deleted = cloudinaryService.deleteImage(publicId);
    return deleted ? new ResponseEntity(HttpStatus.OK)
        : new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @PostMapping(value = "/{publicId}")
  public ResponseEntity<String> post(@PathVariable(required = false) String publicId,
      @RequestParam(value = "filepath") String filePath,
      @RequestParam(value = "secure", required = false) String secureLevel) {

    if (StringUtils.isBlank(secureLevel) ||
           ! Arrays.asList("private", "authenticated", "upload").contains(secureLevel) ) {
      secureLevel = "upload";
    }

    Map response = cloudinaryService.uploadImage(filePath, publicId, secureLevel);
    return response != null ?
        new ResponseEntity(response, HttpStatus.OK)
        : new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
