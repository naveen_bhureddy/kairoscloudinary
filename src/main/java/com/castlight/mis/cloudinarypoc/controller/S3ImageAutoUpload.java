package com.castlight.mis.cloudinarypoc.controller;

import com.castlight.mis.cloudinarypoc.service.CloudinaryService;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@Slf4j
@RequestMapping("s3")
public class S3ImageAutoUpload {

  @Autowired
  private CloudinaryService cloudinaryService;

  @PostMapping(value = "/upload")
  public ResponseEntity<Map> autoUploadAndGetImage(@RequestParam("path") String s3Path) {
     log.info("Got request to Auto upload and get Image " + s3Path);
     Map response = cloudinaryService.uploadURLImage(s3Path, null, "upload");

    return new ResponseEntity<>(response, HttpStatus.OK);
  }

}
