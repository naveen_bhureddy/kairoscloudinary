package com.castlight.mis.cloudinarypoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.castlight"})
public class CloudinaryPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudinaryPocApplication.class, args);
	}
}
