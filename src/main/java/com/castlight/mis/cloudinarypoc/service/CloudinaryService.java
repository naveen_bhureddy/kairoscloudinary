package com.castlight.mis.cloudinarypoc.service;

import com.cloudinary.Cloudinary;
import com.cloudinary.api.ApiResponse;
import com.cloudinary.utils.ObjectUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

@Service
@Slf4j
public class CloudinaryService {

  @Autowired
  private Cloudinary cloudinary;

  public ApiResponse getImages() {
    try {
      return cloudinary.api().resource("", ObjectUtils.emptyMap());
    } catch (Exception ex) {
      log.debug("Exception when invoking cloudinary service. ", ex);
      return null;
    }
  }

  public Map getImage(String publicId) {
    try {
      ApiResponse response = cloudinary.api()
          .resource(publicId, ObjectUtils.emptyMap());
      log.info("Cloudinary response for given publicId :: {} ", response);

      return response;
    } catch (Exception e) {
      log.info("Exception when invoking cloudinary service. ", e);
      throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR,
          "Unable to process the request: " + e.getMessage());
    }
  }

  public boolean deleteImage(String publicId) {
    try {
//  Alternative below -
//    Map response = cloudinary.api()
//          .deleteResources(Collections.singletonList(publicId),
//              ObjectUtils.asMap("type", "upload"));

      //This is using Uploader API - above is using Admin API
      Map response = cloudinary.uploader()
          .destroy(publicId, ObjectUtils.asMap("type", "upload"));
      log.debug("Cloudinary delete response :: " + response.toString());
      return true;

    } catch (Exception e) {
      log.debug("Exception when invoking cloudinary service. ", e);
      return false;
    }
  }

  public Map uploadImage(String filePath, String publicId, String secureLevel) {
    try {
      File f = new File(filePath);

      Map options = new HashMap() {{
        put("type", secureLevel);
      }};

      if (StringUtils.isNotBlank(publicId)) {
        options.put("public_id", publicId);
      }
      return cloudinary.uploader().upload(f, options);

    } catch (Exception e) {
      log.debug("Exception when uploading the image", e);
      return null;
    }
  }

  public Map uploadURLImage(String urlPath, String publicId, String secureLevel) {
    try {
      Map options = new HashMap() {{
        put("type", secureLevel);
      }};
      if (StringUtils.isNotBlank(publicId)) {
        options.put("public_id", publicId);
      }
      return cloudinary.uploader().upload(urlPath, options);

    } catch (Exception e) {
      log.debug("Exception when uploading the image", e);
      return null;
    }
  }

}
