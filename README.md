## Basic details to get started

Cloudinary Account:  naveen.b@imaginea.com / Kairos@123

---
## About this project

There are few end points to play around to upload, retrieve and delete an image from the Cloudinary

* GET:  http://{host:port}/images  - gets all `upload` images from Cloudinary
* GET:  http://{host:port}/images/{publicid}  - get specific image details by public_id
* DELETE:  http://{host:port}/images/{publicid}  - delete the given image from Cloudinary.
* POST: http://{host:port}/images/{publicid}?filepath={}&secure={}  -- uploads the image either securely or publicly
 
The Cloudinary transformation can be applied directly to the URLs that are part of the response

---
## APIs
Uploader API -  for uploading the content to Cloudinary
        https://cloudinary.com/documentation/image_upload_api_reference
        upload, rename, destroy, tags, context (map), explicit, explode, generate_archive, generate_sprite, multi, text

Transformation API - 
   Support different transformation params like Height, Width, Crop, Aspect Ratio, (X, Y) Coordinates, Format, Radius, Quality, 
https://cloudinary.com/documentation/image_transformation_reference

Search API-
    Support different APIs based on PublicId, Folders, tags, Context, Exif, number of faces, predominant Colors

Admin API-
    Provides support to do admin tasks thru REST 

---

## Uploading

//Uploading a local file

        cloudinary.uploader().upload(new File("sample.jpg", ObjectUtils.emptyMap()));

//Uploading a remote file - directly providing the URL

        cloudinary.uploader().upload("http://www.example.com/image.jpg", ObjectUtils.emptyMap());
---

## Security

There are couple of ways to make the image secure - Private and Authenticated. This can be done when uploading the image and setting the “type” option

  //Private
    
    cloudinary.uploader().upload("sample.jpg”, ObjectUtils.asMap("type", "private"));

  //Authenticated
  
    cloudinary.uploader().upload("sample.jpg”, ObjectUtils.asMap("type", “authenticated"));

  **Private:**
* POST response from Cloudinary server

  REQUEST :::
`curl -X POST \
  'http://localhost:8080/images/test?filepath=%2FUsers%2Fnaveenb%2FDesktop%2Ftest.png&secure=private’` 

  RESPONSE  ::: (from cloudinary server)
  ```
  {
    "signature": "90bb4e498c5a534b6f9721c8741d255db75c3fc5",
    "format": "png",
    "resource_type": "image",
    "secure_url": "https://res.cloudinary.com/castlight-health-inc/image/private/s--DsBTO3TS--/v1542875608/test.png",
    "created_at": "2018-11-22T08:33:28Z",
    "type": "private",
    "version": 1542875608,
    "url": "http://res.cloudinary.com/castlight-health-inc/image/private/s--DsBTO3TS--/v1542875608/test.png",
    "public_id": "test",
    "tags": [],
    "original_filename": "test",
    "bytes": 51019,
    "width": 883,
    "etag": "c91ab905b3ea36fb3b21c538c49f641d",
    "placeholder": false,
    "height": 519
  }
  ```
     
  * The original Image is not accessible if tried like this:
    https://res.cloudinary.com/castlight-health-inc/image/private/v1542875608/test.png

  * But accessible with transformations or Secure token
  https://res.cloudinary.com/castlight-health-inc/image/private/w_150,h_150/v1542875608/test.png
      http://res.cloudinary.com/castlight-health-inc/image/private/s--DsBTO3TS--/v1542875608/test.png

**Authenticated:**
* POST response from Cloudinary server

```
{
    "signature": "b8c1a33886fb2e5f260c3a330fab3a49d9a234d7",
    "format": "png",
    "resource_type": "image",
    "secure_url": "https://res.cloudinary.com/castlight-health-inc/image/authenticated/s--DsBTO3TS--/v1542876882/test.png",
    "created_at": "2018-11-22T08:54:42Z",
    "type": "authenticated",
    "version": 1542876882,
    "url": "http://res.cloudinary.com/castlight-health-inc/image/authenticated/s--DsBTO3TS--/v1542876882/test.png",
    "public_id": "test",
    "tags": [],
    "original_filename": "test1",
    "bytes": 39116,
    "width": 911,
    "etag": "569c5960a8acaf407a6839c76e297545",
    "placeholder": false,
    "height": 538
}
```

  * The authenticated Image is not accessible without security token (even for transformation)